import fileinput, json, sys

for line in fileinput.input():
  message = json.loads(line)
  if message.get('reason') == 'compiler-artifact' and message.get('profile').get('test'):
    print(message['executable'])
